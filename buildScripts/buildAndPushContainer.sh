#!/bin/bash

MVN_BUILD_VERSION=$(cat pom.xml | grep "^    <version>.*</version>$" | awk -F'[><]' '{print $3}')

docker login -u "$DOCKER_USERNAME" -p "$DOCKER_PASSWORD"
docker build -t mgabreski/microprofilemeetings:$MVN_BUILD_VERSION .
docker push mgabreski/microprofilemeetings:$MVN_BUILD_VERSION

if [[ $MVN_BUILD_VERSION != *"SNAPSHOT"* ]]; then
    docker tag mgabreski/microprofilemeetings:$MVN_BUILD_VERSION mgabreski/microprofilemeetings:latest
    docker push mgabreski/microprofilemeetings:latest
fi
