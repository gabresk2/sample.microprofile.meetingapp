#!/bin/bash

cd src/test/resources/mongo
docker-compose build --no-cache
docker-compose up -d

cd ../wiremock
docker-compose up -d