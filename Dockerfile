FROM java:alpine
WORKDIR /app
COPY target/microProfileMeetings.jar /app
EXPOSE 9080
ENTRYPOINT ["sh", "-c"]
CMD ["java -jar microProfileMeetings.jar"]