#!/bin/bash

export MONGO_HOST=127.0.0.1
export MONGO_PORT=27017
export DEP1_HOST=127.0.0.1
export DEP1_PORT=5555
export DEP2_HOST=127.0.0.1
export DEP2_PORT=6666
export WIREMOCK_HOST=127.0.0.1
export WIREMOCK_PORT=9090

./buildScripts/startTestContainers.sh

mvn clean verify -P runnable -B

./buildScripts/cleanupContainers.sh