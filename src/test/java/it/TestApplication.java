/*******************************************************************************
 * Copyright (c) 2016 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package it;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.mongodb.*;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.Arrays;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED;
import static org.junit.Assert.*;

public class TestApplication extends EndpointTest {

    public static String applicationUrl;
    public static String dep1Url;
    public static String dep2Url;

    @BeforeClass
    public static void setup(){
        setUpUrls();
        setUpDependencies();
    }

    public static void setUpUrls(){
        String port = System.getProperty("liberty.test.port");
        String war = System.getProperty("war.name");
        applicationUrl = "http://localhost:" + port + "/" + war;

        String dep1Host = System.getenv("DEP1_HOST");
        String dep1Port = System.getenv("DEP1_PORT");
        dep1Url = "http://"+dep1Host+":"+dep1Port;

        String dep2Host = System.getenv("DEP2_HOST");
        String dep2Port = System.getenv("DEP2_PORT");
        dep2Url = "http://"+dep2Host+":"+dep2Port;

        System.out.println("Configured URLs");
    }

    public static void setUpDependencies(){

        loadMongo();

        //Using "external" Wiremock so that it can be a shared resource if team decides to leave it hosted
        WireMock.configureFor(System.getenv("WIREMOCK_HOST"), Integer.parseInt(System.getenv("WIREMOCK_PORT")));
        configureDependency1Mock();
        configureDependency2Mock();

        System.out.println("Configured dependencies");
    }


    private static void loadMongo(){
        //Mongo loading is currently handled by the docker script that launches the mongo container
        //See src/test/resources/mongo

        //This setup is overkill for such a small amount of data, but for huge test data sets (like those I have at work)
        //loading via a dump is faster than performing the load operations in test code

//        try {
//            MongoClient client = new MongoClient(System.getenv("MONGO_HOST"), Integer.parseInt(System.getenv("MONGO_PORT")));
//            DB meetings = client.getDB("meetings");
//            DBCollection coll = meetings.getCollection("meetings");
//            for(int i=1; i<=4; i++){
//                DBObject obj = new BasicDBObject();
//                obj.put("_id", "test_meeting_"+i);
//                obj.put("title", "Test Meeting "+i);
//                obj.put("duration", new Long(i));
//                coll.save(obj);
//            }
//            client.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private static void configureDependency1Mock(){
        WireMock.stubFor(get("/test").willReturn(ok()));

        WireMock.stubFor(any(urlPathEqualTo("/testWithBody")).willReturn(
                aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"result\":\"success\"}")
                        .withStatus(200)
        ));

        WireMock.stubFor(get("/redirect").willReturn(
                temporaryRedirect("/test")
        ));
    }

    private static void configureDependency2Mock() {
        WireMock.stubFor(get("/204").willReturn(aResponse().proxiedFrom("http://httpstat.us")));

        ArrayList<String> ids = new ArrayList<>(Arrays.asList("test1", "test2"));
        for(String id : ids){
            WireMock.stubFor(post("/test").atPriority(1).withRequestBody(containing(id)).willReturn(notFound()));
        }
        WireMock.stubFor(post("/test").atPriority(2).willReturn(ok()));


        WireMock.stubFor(get("/stateful").inScenario("stateful")
                .whenScenarioStateIs(STARTED)
                .willReturn(okJson("1"))
                .willSetStateTo("1")
                );

        WireMock.stubFor(get("/stateful").inScenario("stateful")
                .whenScenarioStateIs("1")
                .willReturn(okJson("2"))
                .willSetStateTo("2")
        );

        WireMock.stubFor(get("/stateful").inScenario("stateful")
                .whenScenarioStateIs("2")
                .willReturn(okJson("3"))
                .willSetStateTo(STARTED)
        );

    }

    @Test
    public void testDeployment() {
        System.out.println("\nTest deployment");
        String endpoint = "/index.html";
        String expectedOutput = "<html data-ng-app='meetings.app'>";
        testGetAndValidatePresent(applicationUrl+endpoint, expectedOutput);
    }

    @Test
    public void executeMeetingsTests(){
        testMeetingsList();
        testMeetingsGet();
        testMeetingsAdd();
        testMeetingsStart();
    }


    public void testMeetingsList(){
        System.out.println("\nTest getting the meetings list");
        String endpoint = "/rest/meetings";
        String expectedOutput = "[{\"id\":\"test_meeting_1\",\"title\":\"Test Meeting 1\",\"duration\":1},{\"id\":\"test_meeting_2\",\"title\":\"Test Meeting 2\",\"duration\":2},{\"id\":\"test_meeting_3\",\"title\":\"Test Meeting 3\",\"duration\":3},{\"id\":\"test_meeting_4\",\"title\":\"Test Meeting 4\",\"duration\":4}]";
        testGetAndValidatePresent(applicationUrl+endpoint, expectedOutput);
    }

    public void testMeetingsGet(){
        System.out.println("\nTest getting a single meeting");
        String endpoint = "/rest/meetings";
        String id = "/test_meeting_1";
        String expectedOutput = "{\"id\":\"test_meeting_1\",\"title\":\"Test Meeting 1\",\"duration\":1}";
        testGetAndValidateEquivalent(applicationUrl+endpoint+id, expectedOutput);
    }

    public void testMeetingsAdd(){
        System.out.println("\nTest adding a meeting");
        String meetingToAdd = "{\"id\":\"test_meeting_5\",\"title\":\"Test Meeting 5\",\"duration\":5}";
        String endpoint = "/rest/meetings";
        Response response = sendRequest(applicationUrl+endpoint, "PUT", meetingToAdd);
        assertEquals(201, response.getStatus());

        testGetAndValidatePresent(applicationUrl+endpoint, meetingToAdd);
    }

    public void testMeetingsStart(){
        System.out.println("\nTest starting a meeting");
        //Add new meeting
        String meetingToAdd = "{\"id\":\"test_meeting_6\",\"title\":\"Test Meeting 6\",\"duration\":1}";
        String endpoint = "/rest/meetings";
        Response response = sendRequest(applicationUrl+endpoint, "PUT", meetingToAdd);
        assertEquals(201, response.getStatus());

        testGetAndValidatePresent(applicationUrl+endpoint, meetingToAdd);

        //Start meeting
        String id = "/test_meeting_6";
        String meetingToStart = "{\"id\":\"test_meeting_6\",\"title\":\"Test Meeting 6\",\"duration\":0.1, \"meetingURL\":\"http://www.example.com/\"}";
        response = sendRequest(applicationUrl+endpoint+id, "POST", meetingToStart);
        assertEquals(204, response.getStatus());

        //Check that URL is stored
        String meetingUrl = "http://www.example.com/";
        testGetAndValidatePresent(applicationUrl+endpoint, meetingUrl);

        //Wait a minute for meeting to end...
        System.out.println("Sleeping 1 min");
        try {
            Thread.sleep(60*1000);
        } catch (InterruptedException e) {
            fail();
        }

        //Now that meeting has ended, check that URL is no longer present
        testGetAndValidateNotPresent(applicationUrl+endpoint, meetingUrl);
    }

    @Test
    public void testSimpleDependency(){
        System.out.println("\nTest simple mocked dependency");
        String endpoint = "/test";
        Response response = sendRequest(dep1Url+endpoint, "GET");
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testDependencyResponse(){
        System.out.println("\nTest simple mocked dependency response body");
        String endpoint = "/testWithBody";
        testGetAndValidateEquivalent(dep1Url+endpoint, "{\"result\":\"success\"}");
    }

    @Test
    public void testDependencyRedirect(){
        System.out.println("\nTest dependency redirect");
        String endpoint = "/redirect";
        Response response = sendRequest(dep1Url+endpoint, "GET");
        assertEquals(302, response.getStatus());
        assertEquals("/test", response.getHeaderString("Location"));
    }

    @Test
    public void testDependencyProxy(){
        System.out.println("\nTest dependency proxy");
        String endpoint = "/204";
        Response response = sendRequest(dep2Url+endpoint, "GET");
        assertEquals(204, response.getStatus());
    }

    @Test
    public void testDependencyRequestMatching(){
        System.out.println("\nTest more advanced request matching");
        String jsonBody = "{\"id\":\"test1\"}";
        String endpoint = "/test";
        Response response = sendRequest(dep2Url+endpoint, "POST", jsonBody);
        assertEquals(404, response.getStatus());

        jsonBody = "{\"id\":\"test2\"}";
        response = sendRequest(dep2Url+endpoint, "POST", jsonBody);
        assertEquals(404, response.getStatus());

        jsonBody = "{\"id\":\"test3\"}";
        response = sendRequest(dep2Url+endpoint, "POST", jsonBody);
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testDependencyStatefulBehaviors(){
        System.out.println("\nTest stateful mocks");
        String endpoint = "/stateful";
        testGetAndValidateEquivalent(dep2Url+endpoint, "1");
        testGetAndValidateEquivalent(dep2Url+endpoint, "2");
        testGetAndValidateEquivalent(dep2Url+endpoint, "3");
        testGetAndValidateEquivalent(dep2Url+endpoint, "1");
    }

    @Ignore
    @Test
    public void failingTest(){
        testGetAndValidatePresent("/rest/meetings", "Error");
    }

}
