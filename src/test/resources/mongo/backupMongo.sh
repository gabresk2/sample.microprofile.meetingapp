#!/bin/bash

if [[ $1 = "" ]] || [[ $2 = "" ]]; then
   echo "Usage: mongoBackup.sh MONGO_CONTAINER_NAME DOCKER_NETWORK_NAME"
   exit -1
fi

mv -f meetingsData.bkup meetingsData.bkup.old
docker run --rm --network="$2" --link $1:mongo -v $(pwd):/backup mongo bash -c 'mongodump --gzip --archive=/backup/meetingsData.bkup --host mongo:27017'