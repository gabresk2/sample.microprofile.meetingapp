# sample.profile.meetingapp
This is a sample app used to demonstrate the use of Eclipse MicroProfile and Liberty (a lighter version of IBM Websphere). I am using this app to explore automated integration testing in isolated containers in the hopes that I can apply my learnings in my workplace.

## Progress
[![Build Status](https://travis-ci.com/mgabreski/sample.microprofile.meetingapp.svg?token=b9XydQvX8GH2w56YLd6U&branch=master)](https://travis-ci.com/mgabreski/sample.microprofile.meetingapp)

So far I have been successful in using TravisCI with this project by creating scripts to deploy the services needed for integration testing. I've also scripted creating the final app container and uploading it to DockerHub. These tasks may be able to be taken care of by Maven plugins (i.e. Fabic8 docker-maven-plugin) and I would like to explore that soon.

My Travis build process uses docker-compose to spin up a mongo container, and then uses another mongo container to load the DB with an archive that is committed in the test resources. The idea is that anytime the tests are updated, the test data load can easily be updated and maintained in the same location. I've added scripts to quickly archive the state of the test DB and add it to the test resources.
 
 I also spin up a docker container that is running Wiremock as a standalone process. This process is used to mock any dependencies required by the service. In this particular case there were no dependencies beyond mongo, but I have explored creating these stubbed services and invoking them in the integration tests.
 
 The build process then uses Maven to build the app and execute all the integration tests by spinning up the app with the liberty-maven-plugin. Once the tests pass and the build is complete, another image is built via the top Dockerfile and the image is pushed to DockerHub.

I also have the whole process working via Jenkins (via Azure). I use Jenkins at my workplace so I wanted to make sure all the work I was doing with Travis would transfer over.

I also want to explore adding another Docker script that will take care of the entire build and test process when running locally, to further reduce dependencies.

## Manual Build
### Prerequisites
* JDK8
* Maven
* Docker

For a successful build you will need the following environment variables configured: 

NOTE: localBuild.sh will configure these for you.
* MONGO_HOST
* DEP1_HOST
* DEP1_PORT
* DEP2_HOST
* DEP2_PORT
* WIREMOCK_HOST
* WIREMOCK_PORT

To push to Dockerhub you will need the following environment variables configured:
* DOCKER_USERNAME
* DOCKER_PASSWORD

### Steps
1. ./localBuild.sh
4. (Optional) ./buildScripts/buildAndPushContainer.sh

(NOTE: To be able to push to DockerHub, you need to provide your credentials as environment variables. You also won't be able to push to DockerHub successfully without modifying the script since its currently targeting my personal repository, but you can change the name/repo tag to push wherever you would like.)

##### From IBM
sample.profile.meetingapp is an e-meeting redirection sample application written using MicroProfile and Liberty.

You can read more about this sample, what it demonstrates, and how to build it yourself in [a series of articles on WASdev.net](https://developer.ibm.com/wasdev/docs/writing-simple-microprofile-application/). You can find out more about [MicroProfile on Liberty on WASdev.net](https://developer.ibm.com/wasdev/docs/microprofile/) and more about MicroProfile on the [official Eclipse MicroProfile project page](https://www.eclipse.org/microprofile). And, for a bit of background, take a look at [microprofile.io](http://microprofile.io/).
